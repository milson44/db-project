<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Artist;
use App\Models\Album;
use App\Models\Track;
use App\Models\MongoArtist;
use Illuminate\Support\Facades\Redis;

class MusicController extends Controller
{
    public function mysqlTest(Request $request)
    {
        //$old_user = DB::connection('musicdb')->table('artists')->get();
//        $artists = DB::select("select * from artists");
//        print_r($artists);
        $artists = Artist::latest('id')->get();
        return view('welcome', ['artists' => $artists]);
    }
    public function index()
    {
        $artists = Artist::latest('id')->get();
        return view('welcome', ['artists' => $artists]);
    }
    public function store(Request $request){
        $res = Artist::create(['name' => $request->name, 'description' => $request->description, 'photo' => $request->photo]);
        $data = ['id' => $res->id, 'name' => $request->title, 'description' => $request->description, 'photo' => $request->photo];
        return $data;
    }
    public function getArtists()
    {
        $artists = Artist::latest('id')->get();
        return response()->json([
            'ok' => true,
            'view' => view('artists', compact('artists'))->render()
        ]);

    }

    public function destroy ($id){
        Artist::find($id)->delete();
        return 'ok';
    }
    public function getArtistsMongo()
    {
        $artists = MongoArtist::all();
        return response()->json(['artists' => $artists]);
    }

    public function getArtistsPSQL()
    {
        $artists = Artist::latest('id')->get();
        return response()->json(['artists' => $artists]);
    }
    public function getArtistsRedis()
    {
        $keys = Redis::keys('*');
        //echo $keys;
        $count = count($keys);

        for($i = 1 ; $i <= $count ; $i ++){
            $artist = Redis::get('artists:'.$i);
            $artist = json_decode($artist, true);
            $artist['id'] = $i;
            $artists[]= $artist;
        }
        return response()->json(['artists' => $artists]);
    }


//    public function generateRedis($id){
//        $names = array(
//            'Christopher',
//            'Ryan',
//            'Ethan',
//            'John',
//            'Zoey',
//            'Sarah',
//            'Michelle',
//            'Samantha',
//        );
//        $surnames = array(
//            'Walker',
//            'Thompson',
//            'Anderson',
//            'Johnson',
//            'Tremblay',
//            'Peltier',
//            'Cunningham',
//            'Simpson',
//            'Mercado',
//            'Sellers'
//        );
//        $texts = array(
//            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
//            'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
//            'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
//            'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth. ',
//            'These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided.'
//        );
//        $random_text = $texts[mt_rand(0, sizeof($texts) - 1)];
//        $randon_photo = str_random(10).'.jpg';
//        for($i=0; $i< 10; $i++){
//            $random_name = $names[mt_rand(0, sizeof($names) - 1)];
//            $random_surname = $surnames[mt_rand(0, sizeof($surnames) - 1)];
//            Redis::set('artists:name', $id, $random_name.' '.$random_surname);
//        }
//    }


//seeder
    public function mongoArtists($data){
        $artist = new MongoArtist();
        $artist->name = $data['name'];
        $artist->description = $data['description'];
        $artist->photo = $data['photo'];
        $artist->save();
    }
    public function artistsDB(){
        $names = array(
            'Christopher',
            'Ryan',
            'Ethan',
            'John',
            'Zoey',
            'Sarah',
            'Michelle',
            'Samantha',
        );
        $surnames = array(
            'Walker',
            'Thompson',
            'Anderson',
            'Johnson',
            'Tremblay',
            'Peltier',
            'Cunningham',
            'Simpson',
            'Mercado',
            'Sellers'
        );
        $texts = array(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
            'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
            'But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth. ',
            'These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided.'
        );
        //Generate a random forename.
        $random_name = $names[mt_rand(0, sizeof($names) - 1)];
        //Generate a random surname.
        $random_surname = $surnames[mt_rand(0, sizeof($surnames) - 1)];
        $random_text = $texts[mt_rand(0, sizeof($texts) - 1)];

        return [
            'name' => $random_name . ' ' . $random_surname,
            'description' => $random_text,
            'photo' => str_random(10).'.jpg'
        ];
    }
    public function tracksDB(){
        return [
            'track_name' => str_random(9),
            'album_id' => rand(1, 1000),
            'genre_id' => rand(1, 9)
        ];
    }


    public function albumsDB(){
        $names = array(
            'Lol',
            'Kek',
            'Cheburek',
            'Sun',
            'Moon',
            'Sarah',
            'Michelle',
            'Gold young',
            'Outsider',
            'Revival',
            'Recobery',
            'Encore',
            'The Slim Shady LP'
        );
        $random_name = $names[mt_rand(0, sizeof($names) - 1)];

        return [
            'artist_id' => rand(1,1000),
            'album_name' => $random_name,
            'year' => rand(1990, 2018)
        ];
    }
    public function postgreArtists($data){
        $artist = new Artist();
        $artist->name = $data['name'];
        $artist->description = $data['description'];
        $artist->photo = $data['photo'];
        $artist->save();
    }
    public function postgreAlbmums($data){
        $artist = new Album();
        $artist->artist_id = $data['artist_id'];
        $artist->album_name = $data['album_name'];
        $artist->year = $data['year'];
        $artist->save();
    }
    public function postgreTracks($data){
        $artist = new Track();
        $artist->track_name = $data['track_name'];
        $artist->album_id = $data['album_id'];
        $artist->genre_id = $data['genre_id'];
        $artist->save();
    }

    public function redisArtists($id , $data){
        Redis::set('artists:'.$id,json_encode($data));
    }
    public function insertDB(){
        for($i =0; $i<1000; $i++) {
            $artists = $this->artistsDB();
            $albums = $this->albumsDB();
            $tracks = $this->tracksDB();
            $this->postgreArtists($artists);
            $this->postgreAlbmums($albums);
            $this->postgreTracks($tracks);
            $this->redisArtists($i,$artists);
            $this->mongoArtists($artists);
        }
        return 'insert 1000 users';
    }

    public function testMongo(){
        $artists = MongoArtist::all();
        return $artists;
    }
//    public function testRedis(){
//        $users = Redis::get('users:'.'1');
//        return $users;
//    }


    public function insertPSQL(){

        $data = $this->artistsDB();
        $artist = new Artist();
        $artist->name = $data['name'];
        $artist->description = $data['description'];
        $artist->photo = $data['photo'];
        $artist->save();

        return response()->json(['artist' => $artist]);
    }

    public function insertRedis(){

        $keys = Redis::keys('*');
        $count = count($keys);
        $data = $this->artistsDB();
        $id = $count + 1;

        Redis::set('artists:'.$id,json_encode($data));
        $artist = Redis::get('users:'.$id);
        $artist = json_decode($artist, true);
        $artist['id' ]= $id;

        return response()->json(['artist' => $artist]);
    }

    public function insertMongo(){

        $data = $this->artistsDB();

        $artist = new MongoArtist();
        $artist->name = $data['name'];
        $artist->description = $data['description'];
        $artist->photo = $data['photo'];
        $artist->save();

        return response()->json(['artist' => $artist]);
    }

}
