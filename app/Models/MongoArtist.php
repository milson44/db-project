<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class MongoArtist extends Eloquent {

    protected $collection = 'artists';
    protected $connection = 'mongodb';
    protected $fillable = ['name','description', 'photo'];

}
