<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    protected $fillable = [

        'artist_id', 'album_name', 'year'

    ];

}
