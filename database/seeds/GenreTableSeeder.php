<?php

use Illuminate\Database\Seeder;

class GenreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genre')->delete();
        DB::table('genre')->insert([
            'genre_name' => 'pop'
        ]);
        DB::table('genre')->insert([
            'genre_name' => 'rap'
        ]);
        DB::table('genre')->insert([
            'genre_name' => 'reggae'
        ]);
        DB::table('genre')->insert([
            'genre_name' => 'rock'
        ]);
        DB::table('genre')->insert([
            'genre_name' => 'dance'
        ]);
        DB::table('genre')->insert([
            'genre_name' => 'opera'
        ]);
        DB::table('genre')->insert([
            'genre_name' => 'techno'
        ]);
        DB::table('genre')->insert([
            'genre_name' => 'jazz'
        ]);
        DB::table('genre')->insert([
            'genre_name' => 'heavy metal'
        ]);

    }
}
