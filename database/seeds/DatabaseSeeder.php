<?php

use Illuminate\Database\Seeder;
use App\Models\Artist;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Model::unguard();
//        $this->call(ArtistsTableSeeder::class);
//        $this->call(AlbumsTableSeeder::class);
//        $this->call(TrackTableSeeder::class);
        $this->call(GenreTableSeeder::class);
    }
}
