<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Базы данных</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">


        <!-- Styles -->
        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

        <script src="{{ asset('js/jquery-2.1.4.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.js') }}"></script>
        <script src="{{asset('js/users.js')}}"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->

        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>

<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    </head>-->
    <body>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!--<audio src="{{ asset('images/music.mp3') }}" autoplay="autoplay"></audio>
    <audio controls>

        <source src="{{ asset('images/music.mp3') }}" type="audio/mpeg">

    </audio>-->

    <h1>Music Databases</h1>
    <div class="container">
        <div class="col-md-4">
            <div class="profile">
                <div class="wrap">

                    <div class="profile-main">
                        <div class="profile-pic wthree">
                            <img src="https://cdn4.iconfinder.com/data/icons/redis-2/1451/Untitled-2-512.png" alt="">
                            <h2>Redis</h2>
                            <p>In-memory key-value store </p>
                        </div>
                        <div class="w3-message">
                            <h5>About Me</h5>
                            <p>Хранит базу данных в оперативной памяти, снабжена механизмами снимков и журналирования для обеспечения постоянного хранения (на диске). </p>
                            <div class="w3ls-touch">
                                <button id="selectAllRedis" type="button" class="btn btn-default">Показать всех</button>

                                <button id="insertRedis" type="button" class="btn btn-default">Добавить запись</button><br><br>
                                <input id="timeSelectAllRedis" style="width: 60px; padding-left: 16px"  disabled/>
                                <input id="timeInsertRedis" style="width: 60px; padding-left: 16px"  disabled/>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="profile">
                <div class="wrap">

                    <div class="profile-main">
                        <div class="profile-pic wthree">
                            <img src="http://artkiev.com/blog/wp-content/uploads/2015/05/mysql.jpg" alt="">
                            <h2>MySQL</h2>
                            <p>Relational database </p>
                        </div>
                        <div class="w3-message">
                            <h5>About Me</h5>
                            <p>MySQL является решением для малых и средних приложений. Входит в состав серверов WAMP, AppServ, LAMP и в портативные сборки серверов Денвер, XAMPP, VertrigoServ.</p>
                            <div class="w3ls-touch">
                                <button id="selectAllMysql" type="button" class="btn btn-default">Показать всех</button>
                                <button id="insertMysql" type="button" class="btn btn-default">Добавить запись</button><br><br>
                                <input id="timeSelectAllMysql" style="width: 60px; padding-left: 16px"  disabled/>

                                <input id="timeInsertMysql" style="width: 60px; padding-left: 16px"  disabled/>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="profile">
                <div class="wrap">
                    <div class="profile-main">
                        <div class="profile-pic wthree">
                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT0RJx4QR34fMEblDph1vftgxMH1YYfJg0MlnYAnea6UDW9UPnA" alt="">
                            <h2>PostgreSQL</h2>
                            <p>Object-relational database </p>
                        </div>
                        <div class="w3-message">
                            <h5>About Me</h5>
                            <p>PostgreSQL создана на основе некоммерческой СУБД Postgres, разработанной как open-source проект в Калифорнийском университете в Беркли.</p>
                            <div class="w3ls-touch">
                                <div class="w3ls-touch">
                                    <button id="selectAllPsql" type="button" class="btn btn-default">Показать всех</button>
                                    <button id="insertPsql" type="button" class="btn btn-default">Добавить запись</button><br><br>
                                    <input id="timeSelectAllPsql" style="width: 60px; padding-left: 16px"  disabled/>

                                    <input id="timeInsertPsql" style="width: 60px; padding-left: 16px"  disabled/>
                                </div>
                                {{--<a href="#" class="load">Select all</a>--}}
                                {{--<a href="#" class="update">Update</a><br>--}}
                                {{--<a type="button" class="" data-toggle="modal" data-target="#addArtist">--}}

                                    {{--Insert--}}

                                {{--</a>--}}
                                {{--<a href="#" class="remove">Remove</a>--}}

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="profile">
                <div class="wrap">

                    <div class="profile-main">
                        <div class="profile-pic wthree">
                            <img src="https://thumbsplus.tutsplus.com/uploads/users/1116/posts/24835/preview_image/mongodb-logo.png?height=300&width=300" alt="">
                            <h2>MongoDB</h2>
                            <p> Document-oriented database </p>
                        </div>
                        <div class="w3-message">
                            <h5>About Me</h5>
                            <p>
                                Классифицирована как NoSQL, использует JSON-подобные документы и схему базы данных. Написана на языке C++.<br></p>
                            <div class="w3ls-touch">
                                <button id="selectAllMongo" type="button" class="btn btn-default">Показать всех</button>

                                <button id="insertMongo" type="button" class="btn btn-default">Добавить запись</button><br><br>
                                <input id="timeSelectAllMongo" style="width: 60px; padding-left: 16px"  disabled/>
                                 <input id="timeInsertMongo" style="width: 60px; padding-left: 16px"  disabled/>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>
    <section>
        <div class="container">
            <h3 id="queryResult"></h3>
            <table class="table table-striped " style="background-color: #fff;color: #000;
    text-align: center;">
                <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>description</th>
                    <th>photo</th>

                </tr>
                </thead>
                <tbody id="mainTable">
                </tbody>
            </table>
        </div>
    </section>
    <div class="modal fade" id="addArtist" tabindex="-1" role="dialog" aria-labelledby="addArtistLabel">

        <div class="modal-dialog" role="document">

            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                    <h4 class="modal-title" id="addArtistLabel">Добавление артиста</h4>

                </div>

                <div class="modal-body">

                    <div class="form-group">

                        <label for="name">Имя артиста</label>

                        <input type="text" class="form-control" id="name">

                    </div>

                </div>

                <div class="modal-body">

                    <div class="form-group">

                        <label for="description">Текст</label>

                        <textarea class="form-control" id="description"></textarea>

                    </div>

                </div>
                <div class="modal-body">

                    <div class="form-group">

                        <label for="photo">Фото</label>

                        <input type="text" class="form-control" id="photo">

                    </div>

                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>

                    <button type="button" class="btn btn-primary" id="save">Сохранить</button>

                </div>

            </div>

        </div>

    </div>

    <div class="container">
        <div class="artists">
    </div>


    </div>
    <script>

        $(function() {

            $('#save').on('click',function(){

                var name = $('#name').val();

                var description = $('#description').val();
                var photo = $('#photo').val();

                $.ajax({

                    url: '{{ route('store') }}',

                    type: "POST",

                    data: {name:name,description:description, photo:photo},

                    headers: {

                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')

                    },

                    success: function (data) {

                        $('#addArtist').modal('hide');

                        $('#artists-wrap').removeClass('hidden').addClass('show');

                        $('.alert').removeClass('show').addClass('hidden');

                        var str = '<tr><td>'+data['id']+

                            '</td><td><a href="/'+data['id']+'">'+data['name']+'</a>'+

                            '</td><td><a href="/'+data['id']+'" class="delete" data-delete="'+data['id']+'">Удалить</a></td></tr>';

                        $('.table > tbody:last').append(str);

                    },

                    error: function (msg) {

                        alert('Ошибка');

                    }

                });

            });

        });
        $('body').on('click','.delete',function(e){

            e.preventDefault();

            var url = $(this).data('href');
            //console.log(url);

            var el = $(this).parents('tr');

            $.ajax({

                url: url,

                type: "POST",

                headers: {

                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                },

                success: function (data) {
                    if(data=="YES"){
                        el.fadeOut().remove();
                    }else{
                        alert("can't delete the row")
                    }
                    /*el.detach();*/

                },

                error:function (xhr, ajaxOptions, thrownError){
                    if(xhr.status==404) {
                        alert(thrownError);
                    }
                }

            });

        });

    </script>
    <script type="text/javascript">
        function getMinTime(min,time) {

            if(!min) min = 10000000;
            if(time < min ) min = time;

            return min;
        }
        $(function() {
            $(document).on('click', '.load', function(event) {
                event.preventDefault();

                $.ajax({
                    url: '/get-artists',
                    type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        if (response.ok = 'true') {
                            $('.artists').html(response.view);
                        }else{
                            alert("can't show ")
                        }
                        /*el.detach();*/

                    },

                    error: function (msg) {

                        alert('Ошибка');

                    }

                });
            });

            $(document).on('click', '.remove', function(event) {
                event.preventDefault();
                $('.artists').html('');
            });
        });
    </script>
    </body>
</html>
