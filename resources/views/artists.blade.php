<table class="table table-striped " style="background-color: #fff;">
    <thead>

    <tr>

        <th>ID</th>

        <th>Имя артиста</th>

        <th>Описание</th>
        <th>Фото</th>


    </tr>

    </thead>

    <tbody>
    @if(count($artists) == 0)
        <tr><td>Записей нет</td></tr>
    @else

    @foreach($artists as $artist)

            <tr>
                <td>{{ $artist->id }}</td>
                <td>{{ $artist->name }}</td>
                <td>{{ $artist->description }}</td>
                <td>{{ $artist->photo }}</td>
                <td><a href="" class="delete" data-href=" {{ route('destroy',$artist->id) }} ">Удалить</a></td>
            </tr>

    @endforeach
    @endif
    </tbody>
</table>

<script>
    $('body').on('click','.delete',function(e){

        e.preventDefault();

        var url = $(this).data('href');
        //console.log(url);

        var el = $(this).parents('tr');

        $.ajax({

            url: url,

            type: "POST",

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            },

            success: function (data) {
                if(data=="YES"){
                    el.fadeOut().remove();
                }else{
                    alert("can't delete the row")
                }
                /*el.detach();*/

            },

            error: function (msg) {

                alert('Ошибка');

            }

        });

    });

</script>

