<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/', 'MusicController@index');
Route::post('/get-artists', 'MusicController@getArtists');

//Route::post('get-artists', 'MusicController@getArtists');
Route::resource('/', 'MusicController',['only' => ['index', 'store', 'show', 'destroy']]);

//Route::get('/redis', 'MusicController@testRedis');
Route::get('/generate/databases', 'MusicController@insertDB');

//TEst get artists
Route::post('/get/artists/mongo', 'MusicController@getArtistsMongo');
Route::post('/get/artists/redis' , 'MusicController@getArtistsRedis');
Route::post('/get/artists/psql' , 'MusicController@getArtistsPSQL');
Route::post('/get/artists/mysql' , 'MusicController@getArtistsPSQL');

//Work get artists
Route::get('/mongo', 'MusicController@getArtistsMongo');
Route::get('/redis' , 'MusicController@getArtistsRedis');
Route::get('/psql' , 'MusicController@getArtistsPSQL');
Route::get('/mysql' , 'MusicController@getArtistsPSQL');


Route::post('/insert/artist/psql' , 'MusicController@insertPSQL');
Route::post('/insert/artist/redis' , 'MusicController@insertRedis');
Route::post('/insert/artist/mongo' , 'MusicController@insertMongo');
Route::post('/insert/artist/mysql' , 'MusicController@insertPSQL');
/*cd redis/redis-stable/src
redis-cli*/

/*
use musicdb;
db.artists.find({}).pretty();
*/

