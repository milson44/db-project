$(document).ready(function () {


    $("#selectAllPsql").on('click', function () {
        start = new Date().getTime();
        $.when(getUsers('psql')).done(function (artists) {
            end = new Date().getTime();
            time = end - start;
            _min_time_selectAllPsql = getMinTime($("#timeSelectAllPsql").val() ,time );
            $("#timeSelectAllPsql").val(_min_time_selectAllPsql);
            fillUsers(artists['artists']);
        });
    });
    $("#selectAllMysql").on('click', function () {

        start = new Date().getTime();

        $.when(getUsers('mysql')).done(function (artists) {

            end = new Date().getTime();
            time = end - start;
            _min_time_selectAllMysql = getMinTime($("#timeSelectAllMysql").val() ,time );
            $("#timeSelectAllMysql").val(_min_time_selectAllMysql);
            fillUsers(artists['artists']);
        });
    });

    $("#selectAllRedis").on('click', function () {

        start = new Date().getTime();

        $.when(getUsers('redis')).done(function (artists) {

            end = new Date().getTime();
            time = end - start;
            _min_time_selectAllRedis = getMinTime($("#timeSelectAllRedis").val() ,time );
            $("#timeSelectAllRedis").val(_min_time_selectAllRedis);

            fillUsers(artists['artists']);
        });

    });

    $("#selectAllMongo").on('click', function () {

        start = new Date().getTime();

        $.when(getUsers('mongo')).done(function (artists) {

            end = new Date().getTime();
            time = end - start;
            _min_time_selectAllMongo = getMinTime($("#timeSelectAllMongo").val() ,time );
            $("#timeSelectAllMongo").val(_min_time_selectAllMongo);
            console.log(artists['artists']);
            fillUsers(artists['artists']);
        });

    });

    $("#insertPsql").on('click', function () {
        start = new Date().getTime();
        $.when(insertUser('psql')).done(function (artist) {
            console.log(artist['name']);
            end = new Date().getTime();
            time = end - start;
            _min_time_insertPsql = getMinTime($("#timeInsertPsql").val() ,time );
            $("#timeInsertPsql").val(_min_time_insertPsql);
             fillUsers(artist);
        });
    });
    $("#insertMysql").on('click', function () {
        start = new Date().getTime();
        $.when(insertUser('mysql')).done(function (artist) {
            console.log(artist['name']);
            end = new Date().getTime();
            time = end - start;
            _min_time_insertMysql = getMinTime($("#timeInsertMysql").val() ,time );
            $("#timeInsertMysql").val(_min_time_insertMysql);
            fillUsers(artist);
        });
    });

    $("#insertRedis").on('click', function () {
        start = new Date().getTime();
        $.when(insertUser('redis')).done(function (artist) {
            end = new Date().getTime();
            time = end - start;
            _min_time_insertRedis = getMinTime($("#timeInsertRedis").val() ,time );
            $("#timeInsertRedis").val(_min_time_insertRedis);
            fillUsers(artist);
        });
    });

    $("#insertMongo").on('click', function () {

        start = new Date().getTime();

        $.when(insertUser('mongo')).done(function (artist) {

            end = new Date().getTime();
            time = end - start;
            // $("#queryResult").text('Mongo query takes ' + time + " ms" );

            _min_time_insertMongo = getMinTime($("#timeInsertMongo").val() ,time );
            $("#timeInsertMongo").val(_min_time_insertMongo);
            fillUsers(artist);
        });
    });

    function getMinTime(min,time) {

        if(!min) min = 10000000;
        if(time < min ) min = time;

        return min;
    }
    
    
    function getUsers(db) {

        return $.ajax({
            type: 'POST',
            url: '/get/artists/' + db,
            data: {
                db: db
            },
            success: function (message) {
                artists = message['artists'];
            },
            error: function (request, status, error) {
                console.log(" >> " + jQuery.parseJSON(request.responseText) ["message"]);
            }
        });
    }
    
    function insertUser(db) {

        return $.ajax({
            type: 'POST',
            url: '/insert/artist/' + db,
            data: {
                db: db
            },
            success: function (message) {
                artist = message['artist'];
            },
            error: function (request, status, error) {
                console.log(" >> " + jQuery.parseJSON(request.responseText) ["message"]);
            }
        });
    }

    function fillUsers(artists) {

        $.each(artists, function (key, artist) {

            key = key + 1;

            $("#mainTable").append(
                "<tr>" +
                "<th>" +
                key +
                "</th>" +

                "<td>" +
                artist['name'] +
                "</td>" +

                "<td>" +
                artist['description'] +
                "</td>" +

                "<td>" +
                artist['photo'] +
                "</td>" +
                "</tr>"
            );
        });

    }

});